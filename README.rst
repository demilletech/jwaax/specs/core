JWAAX Core Spec
===============
| This is the core specification for JSON Web Authentication and Authorization eXchange (JWAAX).
| To build, you will need Pandoc_, and :code:`xml2rfc`.

.. _Pandoc: https://pandoc.org/
