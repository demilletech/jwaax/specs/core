Welcome to JWAAX
================

By demilleTech

JWAAX is an authentication and authorization standard
-----------------------------------------------------

...built for the modern day needs of REST clients and servers. Here is
the implementation doc.  
The AAA acronym stands for "Authentication and Authorization App".

AAA Server
~~~~~~~~~~~~~~~

Let's say you want to provide JWAAX to users. How would you go about
doing that? Well, that's what this is here for.

1. Create a directory, ``.well-known``, at the root of your authorizing
   domain

-  For example, Indra for demilleTech would use
   ``secure.demilletech.net/.well-known/``

2. Create a directory ``jwaax`` inside the ``.well-known`` directory
3. Create a file ``info.json`` inside the previous ``jwaax`` directory
4. Inside the ``info.json`` file, paste the contents below

::

    {
        "aaa_realms": {
            "#YOUR_DOMAIN#": {
                "user_redirect": "#REQUEST_URL#"
            }
        }
    }

For example, DT's Indra info.json file would look as such:

::

    {
        "aaa_realms": {
            "secure.demilletech.net": {
                "user_redirect": "/external/signin"
            }
        }
    }

Consumers
~~~~~~~~~

In order for a consumer to consume JWAAX, it must follow these steps:

1. Create a request token containing the user information
2. Query the AAA's ``.well-known/jwaax/info.json`` and find the
   ``user_redirect`` parameter.
3. Send the user on a redirect to the ``user_redirect`` URL, with the
   token as the ``request_token`` parameter
4. Your user will be redirected to the URL you specified, with ``jt``
   containing their new shiny token.

Example payload for request tokens can be found in the "Request Tokens"
section

Request Tokens
~~~~~~~~~~~~~~

Here is the structure of a request token.

Libraries
^^^^^^^^^

demilleTech is currently working to make reference JWAAX implementations
in various languages. If there is not one available for your language,
feel free to submit a request, or make code for us to review. If there
is not a DT JWAAX implementation available (and you're making a JWAAX
library or simply implementing it in your app), it is highly recommended
that you use a JOSE library that has been audited and has the important
fix of requiring their user to specify the algorithm to be accepted in
decoding.

Header
^^^^^^

The header must be created in compliance with `RFC
7516 <https://tools.ietf.org/html/rfc7516>`__.

It is highly recommended that ``ES512`` is used as the signature
algorithm, with ECDSA/ECDHE . It can be any algorithm the provider
supports, however, ``ES512`` is currently the most secure algorithm
offered by any mainstream libraries.

Payload
^^^^^^^

The payload contains a multitude of claims:

1. Issuer (``iss``) - Must be the domain which is requesting
2. Unique identifier (``jti``) - This is optional, and can be used to
   associate non-authenticated actions on a site with a user, after they
   have authenticated with JWAAX.
3. Issuing Date & Time (``iat``) - This is required, and must be the
   Unix epoch time, in UTC, of the time at which the token was created.
   It is what the time-to-live is based upon.
4. Time-to-live (``ttl``) - The duration for which the token is "alive".
   After this period is over, the token becomes invalid. It is based on
   the ``iat`` parameter.
5. Audience (``aud``) - The domain against which you are requesting. For
   example, to authenticate against DT's Indra, you would use the
   audience ``secure.demilletech.net``
6. Return URL (``returl``) - The URL to which the user will be
   redirected, with the authenticated token. This URL is customizable by
   the consumer, however, some locations may have a requirement that all
   return URLs are registered with the provider. Indra has this
   requirement.
7. Remote Permissions (``remote_permissions``) - Permissions (on remote sites that accept this AAA)
   to be granted to the requesting site. These requested permissions must be shown to the user.

For example, if the domain ``dte.io`` wanted to authenticate a user
against DT's Indra, the following payload would be used:

::

    {
        "iss" : "dte.io",
        "jti" : "kjhg2h51l4538ghkl2k", // Unique temp ID for the user from the consumer. Doesn't get touched by the AAA
        "iat" : 1514753211,
        "ttl" : 30, // seconds
        "aud": "secure.demilletech.net",
        "returl": "dte.io/auth/handle",
        "remote_permissions": {
            "billing.dte.io": [
                "charges.create"
            ]
        }
    }

Signature
^^^^^^^^^

The signature of the requesting token must be created in accordance with
`RFC 7516 <https://tools.ietf.org/html/rfc7516>`__.
